defmodule PhotoServer.Sopen do
  require Logger
  use GenServer

  @photo_dirs %{
    tahr: [
      "/home/polaris/Pictures/**/*.jpg",
      "/home/polaris/various-leprosies/ancient-gulo-1/*.jpg",
      "/home/polaris/various-leprosies/facebook/*.jpg",
      "/home/polaris/various-leprosies/gulo-images/Camera/**/*.jpg",
      "/home/polaris/various-leprosies/gulo-images/OpenCamera/**/*.jpg",
      "/home/polaris/various-leprosies/gulo-camera-2/Camera/**/*.jpg",
      "/home/polaris/various-leprosies/telegram-images/**/*.jpg",
      "/home/polaris/various-leprosies/sibirica-images/Camera/**/*.jpg",
      "/home/polaris/various-leprosies/hranostaj-photos/*.jpg",
      "/home/polaris/various-leprosies/gulo-lento/**/*.jpg",
      "/home/polaris/various-leprosies/gulo-2013/*.jpg",
      "/home/polaris/various-leprosies/gulo-resurrected/Camera/**/*.jpg"
    ],
    "ses-tenxil": [
      "/home/polaris/various-leprosies/gulo-resurrected/Camera/**/*.jpg",
      "/home/polaris/various-leprosies/ancient-gulo-1/*.jpg",
      "/home/polaris/various-leprosies/gulo-images/Camera/**/*.jpg",
      "/home/polaris/various-leprosies/facebook/*.jpg",
      "/home/polaris/various-leprosies/gulo-images/OpenCamera/**/*.jpg",
      "/home/polaris/various-leprosies/gulo-2013/*.jpg",
      "/home/polaris/various-leprosies/gulo-lento/**/*.jpg",
      "/home/polaris/various-leprosies/hranostaj-photos/*.jpg",
      "/home/polaris/various-leprosies/gulo-resurrected/Camera/**/*.jpg",
      "/home/polaris/various-leprosies/sibirica-images/Camera/**/*.jpg",
    ]
  }

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def photo_path(pid) do
    GenServer.call(pid, :photo_path)
  end

  # Server Callbacks
  @impl true
  def init(_incoming) do
    {:ok, h} = :inet.gethostname
    host_key = h |> List.to_string |> String.to_atom
    # Logger.info "photo_dirs: #{inspect @photo_dirs}"
    # Logger.info "Hostname: #{host_key}"
    photo_paths = Enum.map(Map.get(@photo_dirs, host_key, []), fn path ->
      Path.wildcard(path)
    end) |> List.flatten
    # Logger.info "Photo paths: #{inspect photo_paths}"
    {:ok, photo_paths}
  end

  @impl true
  def handle_call(:photo_path, _from, photo_paths) do
    {:reply, Enum.at(photo_paths, :rand.uniform(Enum.count(photo_paths))), photo_paths}
  end
  def handle_call(:thurk, _from, photo_paths) do
    {:reply, "oogleboobie", photo_paths}
  end
end
