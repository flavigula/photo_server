defmodule PhotoServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc "OTP App Thurk for PhotoServer"

  use Application
  alias PhotoServer.Sopen

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: PhotoServer.Worker.start_link(arg)
      # {PhotoServer.Worker, arg}
      {Sopen, name: :photo_server},
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: PhotoServer.Endpoint,
        options: [port: 8997]
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: PhotoServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
