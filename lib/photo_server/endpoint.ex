defmodule PhotoServer.Endpoint do
  use Plug.Router
  plug(:match)
  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)
  plug(:dispatch)

  get "/ping" do
    send_resp(conn, 200, "thurk!")
  end

  get "/photo_path" do
    send_resp(
      conn, 200,
      Poison.encode!(
        %{path: GenServer.call(PhotoServer.Sopen, :photo_path)}
      )
    )
  end
end
