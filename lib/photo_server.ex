defmodule PhotoServer do
  @moduledoc """
  Documentation for `PhotoServer`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> PhotoServer.hello()
      :world

  """
  def hello do
    :world
  end
end
